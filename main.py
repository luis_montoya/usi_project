#!/usr/bin/env python3import time
import serial
import threading
import logging
import time
import socket
import sys, os
import json


HOST = None
PORT = None
COM  = None
BAUD = None
FILE_PATH = None
FILE_NAME = None
FIXTURE_ID =None
STEP = None
EMPLOYEE_NUMBER = None
LINE_NAME = None
CONFIG_FILE = '{}'.format('configurations.json')
# 10.1.2.100

def sn_validate(sn):
    global HOST
    global PORT
    global FILE_PATH
    global FILE_NAME

    if not os.path.exists(FILE_PATH):
        os.mkdir(FILE_PATH)

    if not FILE_PATH.endswith("/"):
        FILE_PATH = '{}/'.format(FILE_PATH)
    out_file = '{}{}'.format(FILE_PATH, FILE_NAME)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        logging.info('Successfully connected to {} port {}'.format(HOST, PORT))
        telegram = '{},{},{},{},{},,OK'.format(FIXTURE_ID, sn.decode().replace("\r\n", ""), STEP, EMPLOYEE_NUMBER, LINE_NAME)
        s.sendall(telegram.encode())
        logging.info('Serial number sent {}'.format(telegram))
        data = s.recv(1024)
        logging.info('Data received from server {}'.format(data))

    with open(out_file, 'w') as response:
        response.write(data.decode().replace("\r\n", ""))
    logging.info('Write response ({}) file ({}) to ICT'.format(data.decode().replace("\r\n", ""), FILE_NAME))

class ReadFromScanner(threading.Thread):

    def run(self):
        global COM
        global BAUD
        ser = serial.Serial(COM, BAUD)
        logging.info('Successfully connected to {} port {}'.format(COM, BAUD))
        while True:
            serial_number = ser.readline().decode()
            if serial_number:
                logging.info('Serial number read {}'.format(serial_number.encode()))
                sn = serial_number.encode()
                sn_validate(sn)
            time.sleep(.1)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, filename='LA.log', filemode='w',
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logger = logging.getLogger(__name__)

    if not os.path.exists(CONFIG_FILE):
        logging.info('File does not exists {}'.format(CONFIG_FILE))
        sys.exit(1)

    with open(CONFIG_FILE, 'r') as config_file:
        data = json.load(config_file)
        HOST = data["host"]
        PORT = data["port"]
        COM = data["com"]
        BAUD = data["baud_rate"]
        debug = data["debug"]
        FILE_PATH = data["response_file_path"]
        FILE_NAME = data["response_file_name"]
        FIXTURE_ID = data["fixture_id"]
        STEP = data["Before_Test_Step"]
        EMPLOYEE_NUMBER = data["employee_number"]
        LINE_NAME = data["line_name"]

    scanner = ReadFromScanner()
    scanner.start()
