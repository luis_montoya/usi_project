import sys
import time
import logging
import os
import shutil

from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from time import sleep
from datetime import datetime
from dataclasses import dataclass

PARSED_FOLDER = 'Parsed'

def move_file(source_path):
    try:
        file_name = os.path.basename(source_path)
        source_path = os.path.dirname(source_path)
        destination_path = f'{source_path}\\{PARSED_FOLDER}\\'
        directory_path = os.path.dirname(destination_path)
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)
        shutil.move(f'{source_path}\\{file_name}', f'{destination_path}\\{file_name}')
    except Exception as exception:
        logging.error(exception.__str__)

@dataclass(init=True)
class Test:
    #id : int
    name : str
    description : str
    file_path: str

    def __init__(self):
        self.description = ''

@dataclass(init=True)
class LogFile:
    #id : int
    file_name : str
    path : str
    created_at : str
    machine : int
    test: int

    def __init__(self):
        self.file_name = ''

@dataclass(init=True)
class LogDetail:
    #id : int
    description : str
    started_at : str
    finished_at : str
    serial_number : str
    status : bool
    result : str
    phase : int
    log_file : int

    def __init__(self):
        self.description = ''

MACHINE_ID = 1
LOGS_PATH = 'C:\Logs'


DATE_FORMAT = '%d-%b-%y %H:%M:%S'
DATE_FORMAT_API = '%Y-%m-%dT%H:%M'

def on_created(event):
    if os.path.dirname(event.src_path) != LOGS_PATH:
        return
    if '.LOG.' in event.src_path:
        sleep(3)

        logging.info(f'Parsing {event.src_path} Teradyne file')
        print(event.src_path)
        log_file = LogFile()
        test = Test()
        log_file.file_name = os.path.basename(path)
        log_file.path = path

        file = open(event.src_path, "r")
        lines = file.readlines()
        lines[:] = [value for value in lines if value != '\n']
        test_info = lines.pop(0).replace('\n', '')
        test_date_index = test_info.index('[') + 1
        test_date_string = test_info[test_date_index:len(test_info)]
        test_path = test_info[0:test_date_index]

        log_file.created_at = datetime.strptime(test_date_string, DATE_FORMAT).strftime(DATE_FORMAT_API)

        test.name = os.path.basename(test_path)
        test.file_path = test_path

        log_details = []
        log_details_test = []

        serial_number = ''
        start_date = datetime.strptime(test_date_string, DATE_FORMAT).strftime(DATE_FORMAT_API)
        for line in lines:
            log_detail = LogDetail()
            log_detail.log_file = 0
            log_detail.phase = 1
            log_detail.status = True
            line = line.replace('\n', '')
            if '@' in line:
                if 'SN' in line:
                    index_serial_number = line.index('SN') + 2
                    serial_number = line[index_serial_number:len(line)].strip()
            elif '(S' in line or '(O' in line or '(B' in line or '(C' in line or '(F' in line:
                log_detail.status = False

                line_index = lines.index(line + '\n')
                description_line = lines[line_index + 2].replace('\n', '')
                result = line[2:len(line)].strip()

                index_serial_number = description_line.index('SN') + 2
                index_date = index_serial_number - 2
                date_string = description_line[1:index_date].strip()
                test_date = datetime.strptime(date_string, DATE_FORMAT).strftime(DATE_FORMAT_API)
                serial_number = description_line[index_serial_number:len(description_line)].strip()

                log_detail.started_at = test_date
                log_detail.finished_at = test_date
                log_detail.result = result
                log_detail.description = line
                log_detail.serial_number = serial_number
                log_details.append(log_detail)
            elif '<' in line or '>' in line:
                log_detail.status = False

                if '<' in line:
                    index_error = line.index('<')
                else:
                    index_error = line.index('>')

                index_error_end = line.index('(')
                result = line[index_error + 1:index_error_end].strip()

                log_detail.result = result
                log_detail.description = line
                log_detail.serial_number = serial_number
                log_details_test.append(log_detail)
            elif '=' in line:
                start_result_index = line.index('=') + 1
                if '(' in line:
                    end_result_index = line.index('(')
                    result = line[start_result_index: end_result_index]
                    if result != '':
                        log_detail.result = result
                        log_detail.description = line
                        log_detail.serial_number = serial_number
                        log_details_test.append(log_detail)
            elif '?' in line or '/' in line:
                finished_date = datetime.strptime(line[1:len(line)], DATE_FORMAT).strftime(DATE_FORMAT_API)
                for log in log_details_test:
                    log.finished_at = finished_date
                    log.started_at = start_date
                log_details.extend(log_details_test)
                log_details_test = []

        for i in log_details:
            print(i)


    elif '.txt' in event.src_path:
        sleep(3)
        logging.info(f'Parsing {event.src_path} 3070 file')
        #parsed_data = _3070.parse_file(event.src_path)

    logging.info(f'Parsing finished for {event.src_path}')
    logging.info(f'Test runned {len(log_details)}')
    logging.info(f'Loading data to server...')
    logging.info(f'Data loaded')
    logging.info(f'Moving file to parsed folder')
    move_file(event.src_path)
    logging.info(f'File moved...')
    logging.info(f'Waiting for another file...')


def on_deleted(event):
    return


def on_modified(event):
    logging.info(f'File modified {event.src_path}')
    return


def on_moved(event):
    return


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, filename='VTX-FRD.log', filemode='w',
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logger = logging.getLogger(__name__)
    logging.info('STARTING SERVICE....')
    patterns = "*"
    ignore_patterns = ""
    ignore_directories = True
    case_sensitive = True
    path = sys.argv[1] if len(sys.argv) > 1 else LOGS_PATH
    event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)
    event_handler.on_created = on_created
    event_handler.on_deleted = on_deleted
    event_handler.on_modified = on_modified
    event_handler.on_moved = on_moved
    observer = Observer()
    observer.schedule(event_handler, path, recursive=False)
    observer.start()
    logging.info('STARTING WATCHDOG....')
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
        observer.join()
